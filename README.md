Основная информация по курсу
---

# Контакты

* Организатор: [Игорь Жирков](http://rubber-duck-typing.com)
   * telegram [@igorjirkov](https://t.me/igorjirkov) / email igorjirkov [at] gmail.com

* [Группа в Telegram](https://t.me/c_lang_cse_2020) (обсуждения, объявления, вопросы)
* [Журнал](https://docs.google.com/spreadsheets/d/17YPxSK7IFSfxbBBI4U0aPcm_vOmqnuSVhHUzH16_4_k/edit?usp=sharing)

# Основные источники материала

* [Класс онлайн-курса на Stepik](https://stepik.org/join-class/a7188388cf6a8a611b530b85db3a6eb9883bd11c) (СУиР: присоединяйтесь к курсу через эту ссылку, пожалуйста)
* [Ссылка на онлайн курс вне класса](https://stepik.org/course/73618)
* [Low-level programming: C, assembly and program execution on Intel 64 architecture](https://www.amazon.com/Low-Level-Programming-Assembly-Execution-Architecture/dp/1484224027) ([искать в Telegram-группе](https://t.me/c/1125766749/3348))
* [Code guidelines, правила хорошего стиля](https://gitlab.se.ifmo.ru/c-language/main/-/wikis/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0-%D1%81%D1%82%D0%B8%D0%BB%D1%8F-%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D1%8F-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC-%D0%BD%D0%B0-C)


# Инструментарий

* `gcc` / `clang` / `msvc`
* `binutils`

# Оценки

- На тройку автоматом достаточно набрать за онлайн-курс 138 баллов из 149 (это 93%). 

   - ВНИМАНИЕ это все модули в сумме!
   - СУиР: обязательно надо зарегистрироваться по ссылке [Класс онлайн-курса на Stepik](https://stepik.org/join-class/bb86771c66a75c230e1501ae25e46901c5413553)

- На четвёрку/пятёрку нужно сделать все задания из онлайн-курса + доп задания:
  - На четвёрку задание одно: [поворот картинки](https://gitlab.se.ifmo.ru/c-language/assignment-image-rotation). 
  - На пятёрку задания два: [поворот картинки](https://gitlab.se.ifmo.ru/c-language/assignment-image-rotation)  и [аллокатор памяти](https://gitlab.se.ifmo.ru/c-language/assignment-memory-allocator). 

- Задания отправлять:
  - Предпочительный способ: сделать форк репозитория с заданием, а для проверки прислать pull request. 

    [Инструкция](https://gitlab.se.ifmo.ru/cse/main/-/wikis/%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D1%81%D0%BB%D0%B0%D1%82%D1%8C-%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0-%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D1%83)

- Отдельного экзамена не будет.
- Оценку за курс можно повышать в будущих семестрах в индивидуальном порядке и без пенальти (но помните про стипендию).

# Сеансы кодирования

* [Пишем Hashmap на С](https://youtu.be/CGY5bJtlvyM)
